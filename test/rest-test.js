const request  = require('supertest');
const expect = require('chai').expect;
const app = require('../js/index');

describe('Test the REST API', () => {

  it('Должен возвращать код 200, сообщение о создании юзера и имя юзера при запросе', () => {
    return request(app).post("/api/v1/users?name=Anna&score=80").then(response => {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(response.text).message).to.equal('New user created!');

      request(app).get("/api/v1/users").then(response => {
        expect(response.statusCode).to.equal(200);
        expect(JSON.parse(response.text)[0].name).to.equal('Anna');
      })
    })
  });

  it('Должен возвращать код 404', () => {
    return request(app).post("/api/v1/userz?name=Anna&score=80").then(response => {
      expect(response.statusCode).to.equal(404);
    })
  });



  it('Должен возвращать код 200 и сообщение об удалении юзера', () => {
    return request(app).delete("/api/v1/users/0").then(response => {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(response.text).message).to.equal('User deleted!');
    })
  })

});
