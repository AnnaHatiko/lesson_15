const expect = require('chai').expect;

const Classes = require('../js/pokemons');

describe('Тест Pokemon', () => {
  describe('Тест метода show', () => {
    const name = 'Pikachy';
    const level = '123';

    it('Должен вернуть строку с переданными параметрами', () => {
      const pokemon = new Classes.Pokemon(name, level);
      const result = pokemon.show();
      expect(result).to.equal('Hi! My name is Pikachy, my level is 123');
    });
    it('Должен вернуть строку с undefined', () => {
      const pokemon = new Classes.Pokemon();
      const result = pokemon.show();
      expect(result).to.equal('Hi! My name is undefined, my level is undefined');
    })
  });
});


describe('Тест Pokemonlist', () => {

  const pokemonList = new Classes.Pokemonlist();
  pokemonList.add('Bulbasar', '190');
  pokemonList.add('Charmander', '28');

  describe('Тест метода add', () => {
    it('Должен вернуть созданного покемона из массива', () => {
      expect(pokemonList[0].show()).to.equal('Hi! My name is Bulbasar, my level is 190')
    });
  });

  describe('Тест метода show Pokemonlist', () => {
    it('Должен вернуть строку с правильным количеством покемонов', () => {
      expect(pokemonList.show()).to.equal('There are 2 pokemons here.');
    })
  });

  describe('Тест метода max Pokemonlist', () => {
    it('Должен вернуть самого сильного покемона', () => {
      const strongest = pokemonList.max();
      expect(strongest.show()).to.equal('Hi! My name is Bulbasar, my level is 190');
    });
  });
});

